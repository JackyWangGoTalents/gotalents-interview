# Interview for GoTalents Frontend Developer
**Note**, there won't be any answers attached.  
**Note**, Please do not have *No this is only for interview, we won't have those problems in real project* thoughts, we **WILL HAVE THOSE ISSUES**  
**Note**, Though it is called **Frontend developer**, but for whoever's using **NodeJS**, those questions are for yours as well.
<br />
## JS Basic
* What's different amone *var*, *let* and *const*?
* Is **null** equals to **undefined**, explain more?
* What's the difference between following function declarations?
```javascript
    function foo() {}
    var foo = function() {}
    var foo = function bar() {}
```
* What's the differentce between *Function.prototype.call* and *Function.prototype.apply*?
* Describe **Ajax** as much as possible.
* Pros and Cons of **Ajax**
* What's difference between **Event Bubbling** and **Event Capture**?
* What's difference between using == and === ?
* When & Why to use `use strict`?
* What is **SPA** and how to make SPA SEO-friendly?
* Explain async/await function, as much as possible.
* What's difference between `function bar(){}` and `bar() => {}`;

## JS Advanced
* Write a `Map` Polyfill, on IE 9.
* What's the output for `[1,2,3].map(ParseInt)`;
* What's the output for `console.log(0.1 + 0.2)`;
* What's the output for 
```
    console.log(1);
    setTimeout(() => { console.log(2) });
    Promise.resolve().then(() => { console.log(3) });
    console.log(4);
```
* What's difference between **Array.prototype.map** and **Array.prototype.forEach**
* Output `console.log( (Math.pow(2, 53)) === (Math.pow(2, 53) + 1) )`;
* What's difference between `Little Endian` and `Big Endian`?

## Frontend Security
* XSS Attack
* CORF Issue

## Regular Expression
* Replace all the numbers to `#` in following string by using regex.
```
    Abcd8s5c9D9CGT83kfdxjs96dglC93516
```

## Others
* What will happen when user enter an URL and press Enter?
* How to increase loading speed for your website?
* Do you know `Shadow DOM`, explain more.
* Do you know `PWA`, explain more.
* What's the new features in `CSS`.
* List all methods to make a element vertically align center.
* Explain `Margin Collapse`.
* What's difference between `strong` tag and `b` tag.
* Why we should add `doctype HTML` at top of the page?

## Projects
* What is `Agile Development`? Pros and Cons?
* How to well organize a project? (open question)

