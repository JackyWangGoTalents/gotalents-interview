# Interview for GoTalents Frontend Developer
**Note**, there won't be any answers attached.  
**Note**, Please do not have *No this is only for interview, we won't have those problems in real project* thoughts, we **WILL HAVE THOSE ISSUES**  
**Note**, Though it is called **Frontend developer**, but for whoever's using **NodeJS**, those questions are for yours as well.
<br />
## JS Basic
* What's different amone *var*, *let* and *const*?  
`let` and `const` are ES6 syntax, the variable initilized by `let` can only works on a specific scope, while the variable declared by `var` can affect globally. `const` means you have to attach a initilized value when you declare the variable. 

* Is **null** equals to **undefined**, explain more?  
`undefined` means the variable is not defined, `null` means the variable is defined, but the value is empty. 

```javascript
    function foo() {}
    var foo = function() {}
    var foo = function bar() {}
```  
* What's the difference between above function declarations?  
`function foo() {}` will declare a function, named `foo`.  
`var foo = function() {}` will declare a variable first, then give a anonymous function as value.   
`var foo = function bar() {}` will give a named function as value.  
key point: [**JavaScript Hoisting**](https://www.w3schools.com/js/js_hoisting.asp)  

* What's the differentce between *Function.prototype.call* and *Function.prototype.apply*?  
[The difference between `call` and `apply`](https://stackoverflow.com/questions/1986896/what-is-the-difference-between-call-and-apply) 

* Describe **Ajax** as much as possible.  
[Ajax](https://www.w3schools.com/xml/ajax_intro.asp) means you can communicate with Server without reloading the entire page. It is the base of modern Web Applications. **Gmail** is a very good example of **Ajax** technology.  

* Pros and Cons of **Ajax**  
pros: mentioned above.  
cons: Not SEO-friendly.   
keypoints: **SEO Strategy**

* What's difference between **Event Bubbling** and **Event Capturing**?  
Event bubbling and capturing are two ways of event propagation in the HTML DOM API, when an event occurs in an element inside another element, and both elements have registered a handle for that event. The event propagation mode determines in which order the elements receive the event.  
With bubbling, the event is first captured and handled by the innermost element and then propagated to outer elements.  
With capturing, the event is first captured by the outermost element and propagated to the inner elements. [from Stackoverflow](https://stackoverflow.com/questions/4616694/what-is-event-bubbling-and-capturing)

* What's difference between using == and === ?  
== is a **permissive value equality**, the compared items will be parsed to the bast format and then compare if their values are identical.  
=== is a **less permissive value equality**. it checks if the value and types are equal of two evaluated items.   
`1 == "1" // true`  
`1 === "1" // false`

* When & Why to use `use strict`?  
By adding `use strict` at top of the source-code.  
The js file will be executed as strict mode, some old or abandoned syntax will be treated as error, while in normal mode we don't normally get errors from them.  

* What is **SPA** and how to make SPA SEO-friendly?  
**SPA** single page application. This question was to check if candidate has solid knowledge of **SEO strategies**, but since Google has supported SPA renderring, this question became a **nice-to-know** question.  

* Explain async/await function, as much as possible.  
`async/await` are syntax sugar for `Promise`.

* What's difference between `function bar(){}` and `bar() => {}`;  
keypoints: [Arrow Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

## JS Advanced
* Write a `Map` Polyfill, on IE 9.
* What's the output for `[1,2,3].map(ParseInt)`;  
The output is `1 NaN NaN`

* What's the output for `console.log(0.1 + 0.2)`;  
`0.3000000000001`
* What's the output for 
```
    console.log(1);
    setTimeout(() => { console.log(2) });
    Promise.resolve().then(() => { console.log(3) });
    console.log(4);
```
```
    1 4 3 2
```


* What's difference between **Array.prototype.map** and **Array.prototype.forEach**
`Array.prototype.map` will execute a given function to each item in the array, and returns a new Array with the same length. `Array.prototype.forEach` will execute a given function to each item, but it will NOT return new array.  

* Output `console.log( (Math.pow(2, 53)) === (Math.pow(2, 53) + 1) )`;  
`true`, keypoints: `Number.MAX_SAFE_INTEGER`

* What's difference between `Little Endian` and `Big Endian`?  
[Check here](https://chortle.ccsu.edu/AssemblyTutorial/Chapter-15/ass15_3.html)


## Frontend Security (open questions)
* XSS Attack
* CORF Issue

## Regular Expression
* Replace all the numbers to `#` in following string by using regex.
```
    Abcd8s5c9D9CGT83kfdxjs96dglC93516
```
```
    "Abcd8s5c9D9CGT83kfdxjs96dglC93516".replace(/\d/g, '#');
```

## Others (Optional questions)
* What will happen when user enter an URL and press Enter?
* How to increase loading speed for your website?
* Do you know `Shadow DOM`, explain more.
* Do you know `PWA`, explain more.
* What's the new features in `CSS`.
* List all methods to make a element vertically align center.
* Explain `Margin Collapse`.
* What's difference between `strong` tag and `b` tag.
* Why we should add `doctype HTML` at top of the page?

## Projects (Optional questions)
* What is `Agile Development`? Pros and Cons?
* How to well organize a project? (open question)

